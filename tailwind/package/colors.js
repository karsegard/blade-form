/*
Definition des couleurs du theme
*/

module.exports =  {
    'primary': '#00B9F1',
    'secondary': '#FF2F97',
    'tertiary': '#FDE53D',
    'neutral': 'black',
    'gli-yellow': '#feff65',
    'gli-pink': '#ff9aff',
    'gli-blue': '#87a7e0',
    'gli-error': '#da3838',
};